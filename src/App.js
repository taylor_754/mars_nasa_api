import logo from './logo.svg';
import './App.css';

class MarsRovers extends Component {
  constructor () {
    super();
    this.handleChange = this.handleChange.bind(this);
  }
  componentWillMount () {
    fetch ("https://pokeapi.co/api/v2/pokemon?limit=150")
    .then(response => {
      if (response.ok)
        {
          return response.json();
        }
      else
      {
        console.log ("data GET failed " + response.status);
        throw Error (response.statusText + " - " + response.url);
      }
    })
      .then(json => {

      })
      .catch (error => {
        console.log (error); 
      });
  }



  render () {

    return (
      <div className="Parent">
      <header className="Header">
        <p>
          Pokemon API app
        </p>
      </header>

      <div className="data-test-container">
        <div className="success-container" style={{display: (success)?"block":"none"}}>
          <select value={this.props.selectedValue} onChange={this.handleChange}>
            {optionItems}
          </select>
          <Pokemon value={{selectedData}}/>
        </div>
        <div className="fail-container" style={{display: (success)?"none":"block"}}>
          <p>
            Failed to load data. Refresh the page to try again.
          </p>
        </div>
      </div>

    </div>
    );
  }


}

export default MarsRovers;
