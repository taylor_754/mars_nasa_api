import React, { Component } from "react";
import Insight from "./Components/Insight.js";
import RoverImages from "./Components/RoverImages.js";
import './App.css';

class MarsRovers extends Component {
  constructor () {
    super();
    this.state = {
      
    }
  }
  componentWillMount () {
  }



  render () {

    return (
      <div className="Parent">

      <div className="body-container">
        <h1>Insight Weather Data</h1>
        <Insight />
        <h1>Mars Rover Latest Raw Images</h1>
        <RoverImages name="Curiosity" />
      </div>

    </div>
    );
  }


}

export default MarsRovers;
