import React, { Component } from "react";
import DataPressureReading from "./DataPressureReading.js";
import '../App.css';

class Insight extends Component {
  constructor () {
    super();
    this.state = {
      sols:[],
      success:false
    }
  }
  componentWillMount () {
    fetch ("https://api.nasa.gov/insight_weather/?api_key="+process.env.REACT_APP_API_KEY+"&feedtype=json&ver=1.0")
    .then(response => {
      if (response.ok)
        {
          this.setState({ success: true });
          return response.json();
        }
      else
      {
        console.log ("data GET failed " + response.status);
        throw Error (response.statusText + " - " + response.url);
      }
    })
      .then(json => {
        console.log (json);
        let sols_data = [];

        json.sol_keys.forEach ( (key) => {
          let sol_entry = json[key];
          sol_entry["sol"] = key;
          sols_data.push(sol_entry);
        });

        this.setState( {sols: sols_data});
        console.log (this.state.sols);

      })
      .catch (error => {
        console.log (error); 
      });
  }



  render () {
    let {success} = this.state;
    let {sols} = this.state;

    let pressureReadingsMap = sols.map ((item, index) => <DataPressureReading value={item} />);

    return (
      <div className="insight-container">
        <div className="success-container" style={{display: (success)?"flex":"none"}}>
            
        {pressureReadingsMap}

        </div>
        <div className="fail-container" style={{display: (success)?"none":"block"}}>
          <p>
            Failed to load data. Refresh the page to try again.
          </p>
        </div>
      </div>
    );
  }


}

export default Insight;
