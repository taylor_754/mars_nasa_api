import React, { Component } from "react";
import RoverImage from "./RoverImage.js";
import '../App.css';

class RoverImages extends Component {
  constructor () {
    super();
    this.state = {
      active:"",
      maxSol:0,
      latestPhotos:[],
      dataSuccess:false
    }
  }
  componentWillMount () {
    fetch ("https://api.nasa.gov/mars-photos/api/v1/rovers/"+this.props.name+"/latest_photos?api_key="+process.env.REACT_APP_API_KEY)
    .then(response => {
      if (response.ok)
        {
          this.setState({ dataSuccess: true });
          return response.json();
        }
      else
      {
        console.log ("data GET failed " + response.status);
        throw Error (response.statusText + " - " + response.url);
      }
    })
      .then(json => {
        console.log (json);
        this.setState({ latestPhotos:json.latest_photos});
        console.log (this.state.latestPhotos);

      })
      .catch (error => {
        console.log (error); 
      });
  }



  render () {
    let {dataSuccess} = this.state;
    let {latestPhotos} = this.state;

    let photos = latestPhotos.map ( (item) => {
      return <RoverImage value={item}/>
    }
    );
    

    return (
      <div className="rover-images-container">
        <div className="success-container" style={{display: (dataSuccess)?"block":"none"}}>

          <h2>{this.props.name}</h2>
          <div className="rover-images-display-container">
            {photos}
          </div>

        </div>
        <div className="fail-container" style={{display: (dataSuccess)?"none":"block"}}>
          <p>
            Failed to load data. Refresh the page to try again.
          </p>
        </div>
      </div>
    );
  }


}

export default RoverImages;
