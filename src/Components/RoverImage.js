import React, { Component } from "react";
import '../App.css';

class RoverImage extends Component {
  constructor () {
    super();
    this.state = {
    }
  }
  componentWillMount () {
    
  }



  render () {
    return (
        <div className="rover-image">
          <h3>{this.props.value.camera.full_name}</h3>
          <p>{this.props.value.earth_date}</p>
          <p>Martian Sol : {this.props.value.sol}</p>
          <img src={this.props.value.img_src} />

        </div>
    );
  }



}

export default RoverImage;