import React, { useState } from 'react';
 
const DataPressureReading = (props) => {

	let sol = props.value.sol;
	let season = props.value.Season;
	let readings = props.value.PRE;

  return ( 
  	<div className="pressure-reading">

  	<h2>Mars Sol {sol}</h2>
  	<h3>{season}</h3>
  	<p>Wind Pressure Readings</p>
  	<p>Average : {readings.av} Pa</p>
  	<p>Readings this Sol : {readings.ct}</p>
  	<p>Min : {readings.mn} Pa</p>
  	<p>Max : {readings.mx} Pa</p>

  </div> 
  );
};
 
export default DataPressureReading;